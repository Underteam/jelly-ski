﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController instance;

    public GameObject player;

    public CharacterController characterController;

    public GameObject ragdoll;

    public GameObject btnRestart;

    public List<WorldGenerator> generators;

    public CollisionTracker tracker;

    public Boat boat;
    public Transform boatModel;

    public Text lblBalance;

    //private int _balance;
    public int balance { get { return PlayerPrefs.GetInt("Balance", 0); } set { lblBalance.text = "" + value; PlayerPrefs.SetInt("Balance", value); } }

    public GameObject btnStart;

    private float invulTimer;
    public GameObject speedeffect;

    public List<GameObject> hideOnStart;

    private float bestResult;

    public GameObject bestResultSign;
    public Text bestResultText;
    public GameObject currResultSign;
    public Text currResultText;
    public GameObject newBestSign;
    public Text newBestText;

    public UpgradeMenu upgradeMenu;

    public GameObject gameOverPanel;
    public Text gameOverText1;
    public Text gameOverText2;
    public Text gameOverText3;
    public GameObject lblOutOfGas;
    public GameObject lblGameOver;

    public Renderer colliderBounds;

    public CopyTransforms copier1;
    public CopyTransforms copier2;
    public Animator anim;

    public RagdollActivator rdActivator;

    private float boatSpeed = 10;

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {

        boat.fuel = UpgradeManager.Instance().GetFuel();
        boat.maxFuel = UpgradeManager.Instance().GetFuel();
        boat.SetSpeed(0);
        boat.enabled = false;
        boat.fuelBar.SetValue(1);

        bestResultSign.SetActive(false);
        currResultSign.SetActive(false);
        newBestSign.SetActive(false);

        colliderBounds.enabled = false;

        copier1.enabled = false;
        copier2.enabled = true;

        boatOriginalPosY = boatModel.transform.position.y;
        playerOriginalPosY = player.transform.position.y;

        gameOverText1.text = "0";

        BestResult();
    }
	
    [EditorButton]
    public void ClearPrefs ()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }

    private void BestResult ()
    {
        bestResult = PlayerPrefs.GetFloat("BestResult", 0);

        if (bestResult > 0)
        {
            bestResultText.text = "" + (int)bestResult;
            bestResultSign.transform.position = new Vector3(-3, bestResultSign.transform.position.y, bestResult);
            bestResultSign.SetActive(true);
        }
        else
            bestResultSign.SetActive(false);

        gameOverText2.text = "BEST " + (int)bestResult;
    }

	// Update is called once per frame
	void Update () {

        boatSpeed = Mathf.Lerp(10f, 20f, player.transform.position.z / 10000);
        if (boat.fuel > 0 && invulTimer <= 0)
        {
            if (boat.speed < boatSpeed) boat.SetSpeed(boatSpeed);
        }

        if(!player.activeSelf && canApply)
        {
            Invoke("ShowGameOver", 1f);

            ApplyResults();
        }

        if(invulTimer > 0)
        {
            invulTimer -= Time.deltaTime;
            if (invulTimer <= 0)
            {
                tracker.invul = false;
                boat.SetSpeed(Mathf.Min(boat._speed, boatSpeed));
                speedeffect.SetActive(false);
                tracker.cube.GetComponent<Renderer>().enabled = true;
            }
        }

        if(canApply) gameOverText1.text = "" + (int)player.transform.position.z;

        //if (player.activeSelf)
        //{
        //    if (player.transform.position.z > bestResult) bestResult = player.transform.position.z;
        //}
    }

    private bool canApply = true;
    private void ApplyResults ()
    {
        Debug.Log("Apply results " + canApply);

        canApply = false;
        currResultText.text = "" + (int)player.transform.position.z;
        Vector3 pos = player.transform.position;// currResultSign.transform.position;
        pos.z = player.transform.position.z-0.5f;
        pos.y = currResultSign.transform.position.y;
        pos.x += 1f;
        //currResultSign.transform.position = pos;

        newBestText.text = "BEST\n" + (int)player.transform.position.z;
        pos.y = newBestSign.transform.position.y;
        //newBestSign.transform.position = pos;

        if (player.transform.position.z > bestResult)
        {
            bestResult = player.transform.position.z;
            newBestSign.SetActive(true);
        }
        else
            currResultSign.SetActive(true);

        gameOverText1.text = "" + (int)player.transform.position.z;
        gameOverText2.text  = "BEST " + (int)bestResult;
        gameOverText3.text = "+" + totalEarned;

        gameOverPanel.SetActive(true);
    }

    public void ShowGameOver ()
    {
        btnRestart.SetActive(true);
    }

    public void FuelEnds ()
    {
        btnRestart.SetActive(true);

        rdActivator.Activate();

        lblOutOfGas.SetActive(true);
        lblGameOver.SetActive(false);

        ApplyResults();
    }

    public void Continue ()
    {
        player.SetActive(true);
        ragdoll.SetActive(false);

        //player.transform.position = Vector3.zero;
        invulTimer = 1f;
        tracker.invul = true;
        tracker.cube.GetComponent<Renderer>().enabled = false;

        btnRestart.SetActive(false);
    }

    public void Restart ()
    {
        player.SetActive(true);
        ragdoll.SetActive(false);

        player.transform.position = Vector3.zero;

        btnRestart.SetActive(false);

        btnStart.SetActive(true);

        for (int i = 0; i < generators.Count; i++) generators[i].Reset();

        float br = PlayerPrefs.GetFloat("BestResult", 0);
        if(bestResult > br) PlayerPrefs.SetFloat("BestResult", bestResult);

        BestResult();

        for (int i = 0; i < hideOnStart.Count; i++) hideOnStart[i].SetActive(true);

        boat.fuel = UpgradeManager.Instance().GetFuel();
        boat.maxFuel = UpgradeManager.Instance().GetFuel();
        boat.SetSpeed(0);
        boat.enabled = false;
        boat.fuelBar.SetValue(1);
        boat.emptyCan.SetActive(false);

        characterController.Input(0);

        upgradeMenu.UpdateInfo();

        currResultSign.SetActive(false);
        newBestSign.SetActive(false);

        invulTimer = 0f;
        tracker.invul = false;
        tracker.cube.GetComponent<Renderer>().enabled = true;

        gameOverPanel.SetActive(false);

        colliderBounds.enabled = false;

        copier1.enabled = false;
        copier2.enabled = true;

        anim.ResetTrigger("Ride");
        anim.Play("Sitting");

        totalEarned = 0;

        Vector3 pos = player.transform.position;
        pos.y = playerOriginalPosY;
        player.transform.position = pos;

        pos = boatModel.transform.position;
        pos.y = boatOriginalPosY;
        boatModel.transform.position = pos;

        lblOutOfGas.SetActive(false);
        lblGameOver.SetActive(true);

        canApply = true;
    }

    private void ChangeCopiers ()
    {
        copier1.enabled = true;
        copier2.enabled = false;
    }

    public void LaunchPad ()
    {
        tracker.invul = true;
        invulTimer = 7f;
        boat.SetSpeed(2 * boatSpeed);
        speedeffect.SetActive(true);
        tracker.cube.GetComponent<Renderer>().enabled = false;
    }

    public void StartGame ()
    {
        boat.fuel = UpgradeManager.Instance().GetFuel();
        boat.maxFuel = boat.fuel;
        boat.SetSpeed(boatSpeed);
        boat.enabled = true;
        btnStart.SetActive(false);

        colliderBounds.enabled = true;

        anim.SetTrigger("Ride");
        Invoke("ChangeCopiers", 1f);

        for (int i = 0; i < hideOnStart.Count; i++) hideOnStart[i].SetActive(false);
    }

    private int totalEarned;
    public void ObstaclePassed ()
    {
        int num = UpgradeManager.Instance().GetEarnings();
        totalEarned += num;
        balance += num;

        num = Mathf.Clamp(num / 3, 3, 10);

        StartCoroutine(AnimateCoins(num));
    }

    public void CoinCollected ()
    {
        totalEarned += 2;
        StartCoroutine(AnimateCoins(2));
    }

    public FlyingCoin coinPrefab;
    public IEnumerator AnimateCoins (int num)
    {
        for(int i = 0; i < num; i++)
        {
            var coin = Instantiate(coinPrefab);
            coin.gameObject.SetActive(true);
            coin.transform.SetParent(coinPrefab.transform.parent);
            coin.transform.position = coinPrefab.transform.position;
            coin.transform.localScale = coinPrefab.transform.localScale;
            coin.amp = Random.Range(5f, 15f);
            coin.w = Random.Range(0f, 360f);
            yield return new WaitForSeconds(0.03f);
        }
    }

    private float boatOriginalPosY;
    private float playerOriginalPosY;
    
    public void OnFuelEnding (float speed)
    {
        if (colliderBounds.enabled) colliderBounds.enabled = false;
        if (speedeffect.activeSelf) speedeffect.SetActive(false);

        //Debug.Log("Fuel ending " + speed + " " + Mathf.Lerp(0f, -1.5f, (1 - speed / 10)));

        Vector3 pos = player.transform.position;
        pos.y = Mathf.Lerp(0f, -1.5f, (1 - speed / 10));
        player.transform.position = pos;

        pos = boatModel.transform.position;
        pos.y = boatOriginalPosY;
        boatModel.transform.position = pos;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Boat : MonoBehaviour {

    public float speed = 5;

    public float fuel = 30;

    public float maxFuel = 30;

    [System.Serializable]
    public class OnFuelEnding : UnityEvent<float> { }
    public OnFuelEnding onFuelEnding;

    public UnityEvent onFuelEnds;

    public float _speed { get; private set; }

    public ProgressBar fuelBar;

    public GameObject emptyCan;

    public GameObject boatWake;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (fuel > 0)
        {
            if (emptyCan.activeSelf) emptyCan.SetActive(false);

            _speed = Mathf.Lerp(_speed, speed, 2 * Time.deltaTime);

            transform.Translate(transform.forward * _speed * Time.deltaTime);

            if (speed > 0) fuel -= Time.deltaTime;

            //if (fuel <= 0 && onFuelEnds != null) onFuelEnds.Invoke();
        }
        else if(_speed > 0.05f)
        {
            if (!emptyCan.activeSelf) emptyCan.SetActive(true);

            _speed = Mathf.Lerp(_speed, 0, 2 * Time.deltaTime);

            transform.Translate(transform.forward * _speed * Time.deltaTime);

            if (_speed <= 0.05f && onFuelEnds != null) onFuelEnds.Invoke();

            if (onFuelEnding != null) onFuelEnding.Invoke(_speed);
        }
        else if(_speed > 0)
        {
            _speed = Mathf.Lerp(_speed, 0, 2 * Time.deltaTime);

            transform.Translate(transform.forward * _speed * Time.deltaTime);
        }

        fuelBar.SetValue(fuel / maxFuel);

        if (fuel > 0)
        {
            if (_speed > 0 && !boatWake.activeSelf) boatWake.SetActive(true);
        }
        else if (boatWake.activeSelf) boatWake.SetActive(false);
    }

    public void SetSpeed (float s)
    {
        speed = s;
        _speed = s;
    }
}

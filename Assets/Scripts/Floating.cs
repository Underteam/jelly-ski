﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour {

    private List<Rigidbody> rbs = new List<Rigidbody>();

    public float waterLevel;

	// Use this for initialization
	void Start () {

        rbs.AddRange(GetComponentsInChildren<Rigidbody>(true));
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
        for(int i = 0; i < rbs.Count; i++)
        {
            float delta = waterLevel - rbs[i].position.y;
            if (delta > 0)
            {
                rbs[i].AddForce((9.8f + 0.2f * delta) * Vector3.up, ForceMode.Acceleration);
            }
        }
	}
}

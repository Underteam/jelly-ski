﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeManager : MonoBehaviour {

    private static UpgradeManager instance;

    public static UpgradeManager Instance ()
    {
        if(instance == null)
        {
            instance = FindObjectOfType<UpgradeManager>();
        }

        if(instance == null)
        {
            GameObject go = new GameObject("UpgradeManager");
            instance = go.AddComponent<UpgradeManager>();
        }

        return instance;
    }

    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(this);
    }

    public int fuelLevel { get { return PlayerPrefs.GetInt("FuelLevel", 0); } set { PlayerPrefs.SetInt("FuelLevel", value); } }

    public int earningsLevel { get { return PlayerPrefs.GetInt("EarningsLevel", 0); } set { PlayerPrefs.SetInt("EarningsLevel", value); } }

    public float GetFuel (int l = -1)
    {
        if(l < 0) l = fuelLevel;

        return 15 + (5 * l);
    }

    public int FuelUpgradePrice ()
    {
        int p1 = 100;
        int p2 = 200;

        if (fuelLevel == 0) return p1;
        if (fuelLevel == 1) return p2;

        for (int i = 0; i < fuelLevel - 1; i++)
        {
            p2 = p1 + p2;
            p1 = p2 - p1;
        }

        return p2;
    }

    public void UpgradeFuel ()
    {
        fuelLevel += 1;
    }

    public int GetEarnings (int l = -1)
    {
        if (l < 0) l = earningsLevel;

        int e = (int)(10 * Mathf.Pow(1.1f, l));

        return e;
    }

    public int EarningsUpgradePrice ()
    {
        int p1 = 100;
        int p2 = 200;

        if (earningsLevel == 0) return p1;
        if (earningsLevel == 1) return p2;

        for(int i = 0; i < earningsLevel - 1; i++)
        {
            p2 = p1 + p2;
            p1 = p2 - p1;
        }

        return p2;
    }

    public void UpgradeEarnings ()
    {
        earningsLevel += 1;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

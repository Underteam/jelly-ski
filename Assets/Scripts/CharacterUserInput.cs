﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterUserInput : MonoBehaviour {

    public CharacterController controller;

    private float input;

    private Vector3 prevPos;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if(Input.GetMouseButtonDown(0))
        {
            prevPos = Input.mousePosition;
        }
        else if(Input.GetMouseButton(0))
        {
            input = controller.input;

            Vector3 delta = Input.mousePosition - prevPos;
            prevPos = Input.mousePosition;

            input -= 0.002f * delta.y;

            controller.Input(input);
        }
	}
}

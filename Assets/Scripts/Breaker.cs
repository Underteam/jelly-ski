﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breaker : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [EditorButton]
    public void Break ()
    {
        List<Rigidbody> rbs = new List<Rigidbody>(GetComponentsInChildren<Rigidbody>());

        Vector3 mainDirection = 1 * Vector3.up + 3 * transform.forward;
        for (int i = 0; i < rbs.Count; i++)
        {
            rbs[i].isKinematic = false;
            rbs[i].velocity = Vector3.zero;
            rbs[i].angularVelocity = Vector3.zero;

            Vector3 dir = 300 * Random.Range(0f, 1f) * Random.onUnitSphere;
            rbs[i].AddForce(mainDirection, ForceMode.VelocityChange);
            rbs[i].AddForce(dir);
            rbs[i].AddTorque(1000 * Random.Range(0f, 1f) * Random.onUnitSphere);
            Debug.Log("Add force to " + rbs[i], rbs[i]);
        }
    }
}

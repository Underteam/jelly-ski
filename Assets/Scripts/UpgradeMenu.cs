﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMenu : MonoBehaviour {

    //public List<GameObject> panels;

    public Text lblBalance;

    public Text lblFuelLevel;

    public Text lblFuel;

    public Text lblFuelUpgradePrice;
    public Text lblFuelUpgradePriceDisabled;

    public Button btnFuelUpgrade;
    public GameObject btnFuelUpgradeDisabled;


    public Text lblEarningsLevel;

    public Text lblEarnings;

    public Text lblEarningsUpgradePrice;
    public Text lblEarningsUpgradePriceDisabled;

    public Button btnEarningsUpgrade;
    public GameObject btnEarningsUpgradeDisabled;

    // Use this for initialization
    void Start () {

        UpdateInfo();
    }

    // Update is called once per frame
    void Update () {

        //TrackCameraPosition();
	}

    public void UpdateInfo ()
    {
        int balance = GameController.instance.balance;

        lblBalance.text = "" + balance;


        lblFuelLevel.text  = "LVL." + (UpgradeManager.Instance().fuelLevel+1);

        lblFuel.text = (int)(100 * (UpgradeManager.Instance().GetFuel() / UpgradeManager.Instance().GetFuel(0))) + "%";

        lblFuelUpgradePrice.text = "" + UpgradeManager.Instance().FuelUpgradePrice();
        lblFuelUpgradePriceDisabled.text = "" + UpgradeManager.Instance().FuelUpgradePrice();

        btnFuelUpgrade.interactable = UpgradeManager.Instance().FuelUpgradePrice() <= balance;

        btnFuelUpgradeDisabled.SetActive(balance < UpgradeManager.Instance().FuelUpgradePrice());


        lblEarningsLevel.text = "LVL." + (UpgradeManager.Instance().earningsLevel+1);

        //Debug.Log(UpgradeManager.Instance().earningsLevel + " " + UpgradeManager.Instance().GetEarnings() + " " + UpgradeManager.Instance().GetEarnings(0));

        lblEarnings.text = (int)(100 * ((float)UpgradeManager.Instance().GetEarnings() / UpgradeManager.Instance().GetEarnings(0))) + "%";

        lblEarningsUpgradePrice.text = "" + UpgradeManager.Instance().EarningsUpgradePrice();
        lblEarningsUpgradePriceDisabled.text = "" + UpgradeManager.Instance().EarningsUpgradePrice();

        btnEarningsUpgrade.interactable = UpgradeManager.Instance().EarningsUpgradePrice() <= balance;

        btnEarningsUpgradeDisabled.SetActive(balance < UpgradeManager.Instance().EarningsUpgradePrice());
    }

    public void UpgradeFuel ()
    {
        GameController.instance.balance -= UpgradeManager.Instance().FuelUpgradePrice();

        UpgradeManager.Instance().UpgradeFuel ();

        UpdateInfo();
    }

    public void UpgradeEarnings ()
    {
        GameController.instance.balance -= UpgradeManager.Instance().EarningsUpgradePrice();

        UpgradeManager.Instance().UpgradeEarnings();

        UpdateInfo();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour {

    public List<Transform> toAppear;

    private float t;

    private int ind;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (ind < toAppear.Count)
        {
            if(!toAppear[ind].gameObject.activeSelf)
            {
                ind++;
                return;
            }

            t += 4 * Time.deltaTime;

            if (t >= 1) t = 1;

            toAppear[ind].localScale = t * Vector3.one;

            if (t == 1) {
                ind++;
                t = 0;
            }
        }
	}

    private void OnEnable()
    {
        for(int i = 0; i < toAppear.Count; i++)
        {
            toAppear[i].localScale = Vector3.zero;
        }

        t = 0;
        ind = 0;
    }
}

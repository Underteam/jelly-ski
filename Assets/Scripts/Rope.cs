﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Rope : MonoBehaviour {

    public LineRenderer rope;

    public Transform point1;

    public Transform point2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (rope == null) return;

        rope.positionCount = 2;

        rope.SetPosition(0, point1.position);
        rope.SetPosition(1, point2.position);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollActivator : MonoBehaviour {

    public Transform rootOfBody;

    public Transform rootOfDoll;

    private struct Pair
    {
        public Transform bone1;
        public Transform bone2;
        public Pair (Transform b1, Transform b2)
        {
            bone1 = b1;
            bone2 = b2;
        }
    }

    private List<Pair> pairs = new List<Pair>();

	// Use this for initialization
	void Start () {

        Prepare();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [EditorButton]
    public void Prepare ()
    {
        pairs.Clear();

        pairs.Add(new Pair(rootOfBody, rootOfDoll));

        List<Transform> list1 = new List<Transform>();

        List<Transform> list2 = new List<Transform>();

        int ind = 0;

        while (ind < pairs.Count)
        {
            var tr1 = pairs[ind].bone1;
            var tr2 = pairs[ind].bone2;

            list1.Clear();
            list2.Clear();

            for (int i = 0; i < tr1.childCount; i++) list1.Add(tr1.GetChild(i));
            for (int i = 0; i < tr2.childCount; i++) list2.Add(tr2.GetChild(i));

            while(list1.Count > 0)
            {
                for(int j = 0; j < list2.Count; j++)
                {
                    if (list2[j].name.Equals(list1[0].name))
                    {
                        pairs.Add(new Pair(list1[0], list2[j]));
                        list2.RemoveAt(j);
                        break;
                    }
                }
                list1.RemoveAt(0);
            }

            ind++;
        }

        Debug.Log("Found " + pairs.Count + " matches");
    }

    public void Activate ()
    {
        for(int i = 0; i < pairs.Count; i++)
        {
            pairs[i].bone2.localPosition = pairs[i].bone1.localPosition;
            pairs[i].bone2.localRotation = pairs[i].bone1.localRotation;
        }

        rootOfBody.gameObject.SetActive(false);
        rootOfDoll.gameObject.SetActive(true);
    }
}

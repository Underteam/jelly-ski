﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    public Rigidbody bulletPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //RaycastHit hit;
            //if(Physics.Raycast(ray, out hit, 10))
            {
                var bullet = Instantiate(bulletPrefab);
                bullet.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                bullet.velocity = 30 * ray.direction;
            }
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyingCoin : MonoBehaviour {

    public Transform target;
    public float moveSpeed = 2;
    public float amp = 10;
    public float scaleDownDist = 10;

    private Vector3 scale;

    public float w = 0;

    private float length;

    public bool inited;

    private float sign = 1;

    // Use this for initialization
    private void Init()
    {
        if (target == null) return;

        Vector3 dir = target.position - transform.position;

        float dist = dir.magnitude;

        moveSpeed = moveSpeed * dist;

        w = 15 / moveSpeed;

        w = Random.Range(0.7f * w, 1.3f * w);

        scale = transform.localScale;

        inited = true;

        if (Random.Range(0f, 1f) < 0.5f) sign = -1;
    }

	// Update is called once per frame
	void Update () {

        if (!inited) Init ();

        if (target != null)
        {
            Vector3 dir = target.position - transform.position;
            float dist = dir.magnitude;
            dir.Normalize();
            Vector3 perp = Vector3.Cross(dir, Vector3.forward);

            float a = Mathf.Clamp(dist, 0, scaleDownDist) / scaleDownDist;

            transform.localScale = scale * a;

            Vector3 speed = dir * moveSpeed;

            float delta = speed.magnitude * Time.unscaledDeltaTime;

            length += delta;

            if(delta > dist)
            {
                Destroy(gameObject);
                //gameObject.SetActive(false);
            }

            transform.position += speed * Time.unscaledDeltaTime + amp * perp * Mathf.Sin(w * length) * a * sign;
        }
    }
}

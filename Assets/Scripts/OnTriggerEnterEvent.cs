﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggerEnterEvent : MonoBehaviour {

    [System.Serializable]
    public class OnEnter : UnityEvent<Collider> { }

    public OnEnter onEnter;

	// Use this for initialization
	void Start () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (onEnter != null) onEnter.Invoke(other);
    }
}

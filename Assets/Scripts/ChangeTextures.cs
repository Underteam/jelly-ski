﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTextures : MonoBehaviour {

    public Renderer rend1;
    public Renderer rend2;

    private float timer = 0.3f;

	// Use this for initialization
	void Start () {

        rend1.enabled = true;
        rend2.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            rend1.enabled = !rend1.enabled;
            rend2.enabled = !rend2.enabled;
            timer = 0.3f;
        }
	}
}

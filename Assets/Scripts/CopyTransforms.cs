﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyTransforms : MonoBehaviour {

    [System.Serializable]
    public class Pair
    {
        public string name;
        public Transform copyFrom;
        public Transform copyTo;
        public bool lateUpdate;
    }

    public Pair armature;

    public List<Pair> pairs;

	// Use this for initialization
	void Start () {
		
	}

    private void Update()
    {
        for (int i = 0; i < pairs.Count; i++)
        {
            if (pairs[i].lateUpdate) continue;
            pairs[i].copyTo.localPosition = pairs[i].copyFrom.localPosition;
            pairs[i].copyTo.localRotation = pairs[i].copyFrom.localRotation;
        }
    }

    // Update is called once per frame
    void LateUpdate () {

        Vector3 pos = armature.copyTo.localPosition;
        pos.y = armature.copyFrom.localPosition.y;
        armature.copyTo.localPosition = pos;

        for (int i = 0; i < pairs.Count; i++)
        {
            if (!pairs[i].lateUpdate) continue;
            pairs[i].copyTo.localPosition = pairs[i].copyFrom.localPosition;
            pairs[i].copyTo.localRotation = pairs[i].copyFrom.localRotation;
        }
	}

    public void HardReverse ()
    {
        for (int i = 0; i < pairs.Count; i++)
        {
            pairs[i].copyFrom.localPosition = pairs[i].copyTo.localPosition;
            pairs[i].copyFrom.localRotation = pairs[i].copyTo.localRotation;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.Dynamics;

public class Impactor : MonoBehaviour {

    public List<GameObject> switchOn;

    public List<GameObject> switchOff;

    public PuppetMaster pm;

    private List<Rigidbody> rbs = new List<Rigidbody>();

    public RagdollActivator rdActivator;

	// Use this for initialization
	void Start () {

        rbs.AddRange(pm.GetComponentsInChildren<Rigidbody>());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [EditorButton]
    public void Impact ()
    {
        {
            rbs.Clear();
            rbs.AddRange(rdActivator.rootOfDoll.GetComponentsInChildren<Rigidbody>());
            rdActivator.Activate();

            Vector3 mainDirection = 5 * Vector3.up + 3 * transform.forward;
            for (int i = 0; i < rbs.Count; i++)
            {
                rbs[i].velocity = Vector3.zero;
                rbs[i].angularVelocity = Vector3.zero;

                Vector3 dir = 300 * Random.Range(0f, 1f) * Random.onUnitSphere;
                rbs[i].AddForce(mainDirection, ForceMode.VelocityChange);
                rbs[i].AddForce(dir);
                rbs[i].AddTorque(1000 * Random.Range(0f, 1f) * Random.onUnitSphere);
                Debug.Log("Add force to " + rbs[i], rbs[i]);
            }
        }
    }

    public void ApplyForces ()
    {
        Vector3 mainDirection = 500 * Vector3.up;
        for (int i = 0; i < rbs.Count; i++)
        {
            Vector3 dir = 70 * Random.Range(0f, 1f) * Random.onUnitSphere;
            rbs[i].AddForce(mainDirection + dir);
            //Debug.Log("Add force to " + rbs[i], rbs[i]);
        }
    }
}

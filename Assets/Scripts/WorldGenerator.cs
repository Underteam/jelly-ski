﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator : MonoBehaviour {

    public Transform player;

    public float generationDistance = 100;

    //public float prefabLength;

    //public List<GameObject> prefabs;

    private float lastGeneratedDist;

    private List<GameObject> generated = new List<GameObject>();
    private List<Prefab> references = new List<Prefab>();

    public float startDist;

    public bool dontRepeat;

    private Prefab lastGeneratedPrefab;

    [System.Serializable]
    public class Prefab
    {
        public string name;
        public GameObject prefab;
        public Vector2 nextObstacleDist;
        public float size;
        public float rarity;
        public Vector2 appearanceDistance;
    }

    public List<Prefab> prefabs2;

	// Use this for initialization
	void Start () {

        lastGeneratedDist = startDist;
    }
	
	// Update is called once per frame
	void Update () {

        Generate();
    }

    public void RemoveUnused ()
    {

        //float maxDist = 10;
        //if (prefabLength > 0) maxDist = 2 * prefabLength;

        while(generated.Count > 0 && (player.position - generated[0].transform.position).z > Mathf.Max(10, 2 * Mathf.Abs(references[0].size)))
        {
            Destroy(generated[0]);
            generated.RemoveAt(0);
            references.RemoveAt(0);
        }
    }

    public bool log;

    public void Generate ()
    {
        List<Prefab> list = new List<Prefab>();
        List<float> chances = new List<float>();

        while (lastGeneratedDist - player.position.z < generationDistance)
        {
            list.Clear();
            chances.Clear();
            float sum = 0;
            for(int i = 0; i < prefabs2.Count; i++)
            {
                Vector2 ad = prefabs2[i].appearanceDistance;
                bool canBePlaced = ad.x <= 0 &&ad.y <= 0;
                if (!canBePlaced) {
                    if (ad.x > 0 && ad.y > 0) canBePlaced = lastGeneratedDist >= ad.x && lastGeneratedDist <= ad.y;
                    else if (ad.y <= 0) canBePlaced = lastGeneratedDist >= ad.x;
                    else if (ad.x <= 0) canBePlaced = lastGeneratedDist <= ad.y;
                }
                if (canBePlaced)
                {
                    list.Add(prefabs2[i]);
                    chances.Add(prefabs2[i].rarity);
                    sum += prefabs2[i].rarity;
                }
            }

            if (log) Debug.Log(lastGeneratedDist + " " + list.Count + " " + chances.Count + " " + sum);

            var prefab = GetRandomPrefab(list, chances, sum);
            while (dontRepeat && list.Count > 1 && prefab == lastGeneratedPrefab) prefab = GetRandomPrefab(list, chances, sum);
            if (log) Debug.Log(prefab.prefab, prefab.prefab);
            lastGeneratedPrefab = prefab;
            var o = Instantiate(prefab.prefab);
            generated.Add(o);
            references.Add(prefab);

            o.SetActive(true);

            Vector3 pos = prefab.prefab.transform.position;

            if(prefab.nextObstacleDist.x == 0 && prefab.nextObstacleDist.y == 0)
            {
                pos.z = lastGeneratedDist;
                lastGeneratedDist += Random.Range(5f, 10f);
            }
            else if (prefab.nextObstacleDist.y > 0)
            {
                float min = Mathf.Max(prefab.nextObstacleDist.x, 0);
                float max = prefab.nextObstacleDist.y;
                if(min > max)
                {
                    max = min;
                    min = prefab.nextObstacleDist.y;
                }
                pos.z = lastGeneratedDist;
                lastGeneratedDist += Random.Range(min, max);
            }

            if (prefab.size < 0) pos.z += prefab.size;
            o.transform.position = pos;
            o.transform.rotation = prefab.prefab.transform.rotation;
        }

        RemoveUnused();
    }

    private Prefab GetRandomPrefab (List<Prefab> prefabs, List<float> chances, float sum)
    {
        if (prefabs.Count == 0) return null;
        float rnd = Random.Range(0f, sum);
        int ind = 0;
        while (ind < prefabs.Count && rnd > chances[ind])
        {
            rnd -= chances[ind];
            ind++;
        }

        if (ind >= prefabs.Count) ind--;

        return prefabs[ind];
    }

    public void Reset()
    {
        lastGeneratedDist = startDist;

        for (int i = 0; i < generated.Count; i++) Destroy(generated[i]);

        generated.Clear();
        references.Clear();

        Generate();
    }
}

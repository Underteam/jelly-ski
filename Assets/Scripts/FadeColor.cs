﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeColor : MonoBehaviour {

    public Material mat;

    private List<Renderer> rends = new List<Renderer>();

    private Vector3 scale;

    private float timer = 1f;

    private List<Color> colors = new List<Color>();

    private float speed = 5f;

	// Use this for initialization
	void Start () {

        rends.AddRange(GetComponentsInChildren<Renderer>());
        for(int i = 0; i < rends.Count; i++) rends[i].material = mat;
        scale = transform.localScale;
        for(int i = 0; i < rends.Count; i++) colors.Add(rends[i].material.color);
	}
	
	// Update is called once per frame
	void Update () {

        timer -= speed * Time.deltaTime;
        if(timer <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            transform.localScale = scale * (1 + 1f * (1 - timer));
            for (int i = 0; i < rends.Count; i++)
            {
                Color c = colors[i];
                c.a = colors[i].a * timer;
                rends[i].material.color = c;
            }
        }
	}
}

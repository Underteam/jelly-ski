﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {

    public float input { get; private set; }

    public Animator anim;

    public Transform pelvis;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Input (float inp)
    {
        input = inp;

        if (input < 0) input = 0;
        if (input > 1) input = 1;

        anim.SetFloat("Height", input);

        float h = -0.8f * (1 - Mathf.Cos(Mathf.Deg2Rad * ((86-2) * input + 2)));

        Vector3 pos = pelvis.localPosition;
        pos.y = h;
        pelvis.localPosition = pos;
    }
}

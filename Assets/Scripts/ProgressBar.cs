﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {

    public float minVal = 0f;

    public float maxVal = 1f;

    //[Range(0f, 1f)]
    //public float val;

	public Image fill;

    public RectTransform indicator;

	public void SetValue(float val) {
	
		val = Mathf.Clamp (val, 0, 1);
		fill.fillAmount = minVal + val * (maxVal-minVal);

        if(indicator != null) indicator.anchoredPosition = new Vector2(fill.fillAmount * fill.rectTransform.rect.width, 0);
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shaker : MonoBehaviour {

    private List<Rigidbody> listOfRB = new List<Rigidbody>();

    private float timer;

    // Use this for initialization
    void Start () {

        listOfRB.AddRange(GetComponentsInChildren<Rigidbody>());
    }
	
	// Update is called once per frame
	void FixedUpdate () {

        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = Random.Range(0.2f, 0.5f);
            for (int i = 0; i < listOfRB.Count; i++)
            {
                Vector3 dir = 70 * Random.Range(0f, 1f) * Random.onUnitSphere;
                listOfRB[i].AddForce(dir);
            }
        }


        //for(int i = 0; i < listOfRB.Count; i++)
        //{
        //    Vector3 dir = 10 * delta.y * Random.Range(0f, 1f) *  Random.onUnitSphere;
        //    listOfRB[i].AddForce(dir);
        //}
    }
}

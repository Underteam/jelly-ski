﻿using RootMotion.Dynamics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionTracker : MonoBehaviour {

    public Transform player;

    public Transform plane;
    public Transform cube;

    public Transform col;
    public Transform leftLegCol;
    public Transform rightLegCol;

    public LayerMask mask;

    public Transform headBone;
    public Transform leftFootBone;
    public Transform leftThighBone;
    public Transform rightFootBone;
    public Transform rightThighBone;

    private Vector3 planeScale;

    private Vector3 colScale;

    public UnityEvent onCollision;

    public UnityEvent onLaunchPad;

    public UnityEvent onObstaclePassed;

    public bool invul;

    public Material mat;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {

        if (obstacle == null) obstacle = nextObstacle;
        else {
            if (obstacle.position.z + 0.05f <= player.position.z)
            {
                if (obstacle != prevObstacle)
                {
                    prevObstacle = obstacle;
                    var clone = Instantiate(obstacle, obstacle.position, obstacle.rotation);
                    clone.localScale = obstacle.localScale;
                    clone.gameObject.AddComponent<FadeColor>().mat = mat;
                    var list = clone.GetComponentsInChildren<Collider>();
                    for (int i = 0; i < list.Length; i++) list[i].enabled = false;

                    Debug.Log(obstacle + " " + prevObstacle, prevObstacle);
                    Debug.Log("Clone " + clone, clone);

                    if (onObstaclePassed != null) onObstaclePassed.Invoke();

                    obstacle = null;
                }
            }
        }
	}

    private Vector3 boxcasterSize = new Vector3(1.5f, 1.5f, 0.2f);
    public Transform nextObstacle;
    private Transform prevObstacle;
    private Transform obstacle;

    private void FixedUpdate()
    {
        nextObstacle = null;
        RaycastHit hit;
        if (Physics.BoxCast (player.position, boxcasterSize, player.forward, out hit, player.rotation, 100, mask))
        {
            if (hit.transform != prevObstacle) nextObstacle = hit.transform.parent;
        }
        else
        {
            nextObstacle = null;
        }

        if (nextObstacle != null) while (nextObstacle.parent != null) nextObstacle = nextObstacle.parent;

        //if (nextObstacle == prevObstacle) nextObstacle = null;
        //else prevObstacle = nextObstacle;
    }

    private void LateUpdate()
    {
        {
            float l = (leftThighBone.position - leftFootBone.position).magnitude;
            Vector3 s = leftLegCol.localScale;
            s.y = l;
            leftLegCol.position = (leftThighBone.position + leftFootBone.position) / 2;
            leftLegCol.rotation = Quaternion.LookRotation(transform.forward, (leftThighBone.position - leftFootBone.position).normalized);
        }

        {
            float l = (rightThighBone.position - rightFootBone.position).magnitude;
            Vector3 s = rightLegCol.localScale;
            s.y = l;
            rightLegCol.position = (rightThighBone.position + rightFootBone.position) / 2;
            rightLegCol.rotation = Quaternion.LookRotation(transform.forward, (rightThighBone.position - rightFootBone.position).normalized);
        }

        if(nextObstacle != null)
        {
            plane.gameObject.SetActive(true);
            cube.gameObject.SetActive(true);
            Vector3 pos = player.transform.position;
            pos.y = center;
            pos.z = nextObstacle.position.z;
            plane.position = pos;

            planeScale.z = Mathf.Max(pos.z - player.position.z - 0.5f, 0.01f);
            cube.localScale = planeScale;
            pos.z = (pos.z + player.position.z + 0.5f) / 2;
            cube.position = pos;
        }
        else
        {
            plane.gameObject.SetActive(false);
            cube.gameObject.SetActive(false);
            Vector3 pos = player.transform.position;
            pos.y = center;
            pos.z += 5;
            plane.position = pos;
        }

        CalculateBounds();

        {
            Vector3 pos = col.localPosition;
            pos.y = center;
            col.localPosition = pos;
        }
    }

    private float center;
    public void CalculateBounds ()
    {
        Vector3 headPos = headBone.position;

        Vector3 lfPos = leftFootBone.position;

        Vector3 rfPos = rightFootBone.position;

        float height = Mathf.Abs((headPos - lfPos).y);

        float width = Mathf.Abs((lfPos - rfPos).x) + 0.05f;

        center = (lfPos.y + headPos.y) / 2;

        planeScale.x = width;
        planeScale.y = height;
        planeScale.z = 1;

        plane.localScale = planeScale;

        colScale.x = Mathf.Max(0.5f, width);
        colScale.y = height;
        colScale.z = 0.25f;

        col.localScale = colScale;
    }

    private void OnTriggerEnter(Collider other)
    {
        Coin coin = other.GetComponent<Coin>();
        if(coin != null)
        {
            GameController.instance.CoinCollected();
            Destroy(coin.gameObject);
            return;
        }

        if(other.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
        {
            if (!invul && onCollision != null) onCollision.Invoke();
            else if(invul)
            {
                var breaker = other.GetComponentInParent<Breaker>();
                if (breaker != null) breaker.Break();
            }
        }
        else if(other.gameObject.layer == LayerMask.NameToLayer("LaunchPad"))
        {
            if (onLaunchPad != null) onLaunchPad.Invoke();
        }
    }

    public void OnTriggerEnterExternal (Collider other)
    {
        //Debug.Log("Enter " + other, other);
        OnTriggerEnter(other);
    }

    private void OnDisable()
    {
        if(cube != null) cube.gameObject.SetActive(false);
        if(plane != null) plane.gameObject.SetActive(false);
    }
}

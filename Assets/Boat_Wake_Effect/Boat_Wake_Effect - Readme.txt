This Boat Wake Effect pack provides an animatated water wake effect for your boats.

Load the Boat_Wake_Effect.unity for an example scene.

Two separate solutions to the effect are provided; if your boat doesn't need to turn, use the boat wake particles. However, if you boat does need to change axis, use the UV animated versions (AnimatedWake_Front and AnimatedWake_Back prefabs)

Have fun!

If you have any questions then please get in touch at bendurrant@rivermillstudios.com



